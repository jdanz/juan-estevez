package com.tienda.online.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModeloNotFoundException;
import com.tienda.online.models.Usuario;
import com.tienda.online.services.UsuarioService;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {

	private UsuarioService usuarioService;

	public UsuarioController(UsuarioService usuarioService) {
		super();
		this.usuarioService = usuarioService;
	}

	@PostMapping
	public ResponseEntity<Usuario> guardar(@RequestBody @Validated Usuario usuario) {
		Usuario newUsuario = usuarioService.guardar(usuario);
		if (newUsuario == null) {
			throw new DataIntegrityViolationException("Ya existe un usuario con email: " 
							+ usuario.getEmail());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newUsuario);
	}

	@GetMapping
	public ResponseEntity<List<Usuario>> listar() {
		List<Usuario> lista = usuarioService.listar();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Usuario> actualizar(@RequestBody @Validated Usuario usuario) {
		Usuario updateUsuario = usuarioService.actualizar(usuario);
		return new ResponseEntity<Usuario>(updateUsuario, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> obtenerPorId(@PathVariable(value="id") Integer id) {
		Usuario usuario = usuarioService.obtenerPorId(id);
		if(usuario == null) {
			throw new ModeloNotFoundException("Usuario con id: " + id + " no se encontro");
		}
//		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
		return ResponseEntity.ok(usuario);
	}

}











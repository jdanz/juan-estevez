package com.tienda.online.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Column(name = "email", nullable = false, length = 70)	
	private String email;
	
	@Size(min=3,max=150,message="Nombre debe tener mínimo 3 caracteres y máximo 150")
	@Column(name = "nombres", nullable = false, length = 150)	
	private String nombres;
	
	@Size(min=3,max=150,message="Apellido debe tener mínimo 3 caracteres y máximo 150")
	@Column(name = "apellidos", nullable = false, length = 150)
	private String apellidos;	
	
	@Size(min=3,max=150,message="Password debe tener mínimo 3 caracteres y máximo 50")
	@Column(name = "password", nullable = false, length = 150)
	private String password;
	private String telefono;
	private String direccion;
	private Date fecha;

	@ManyToOne
	@JoinColumn(name = "rol_id")
	private Rol rol;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

}

package com.tienda.online.services;

import java.util.List;
import org.springframework.stereotype.Service;
import com.tienda.online.models.Articulo;
import com.tienda.online.repositories.ArticuloRepository;

@Service
public class ArticuloService {
	private ArticuloRepository articuloRepository;

	public ArticuloService(ArticuloRepository articuloRepository) {
		this.articuloRepository = articuloRepository;
	}
	
	
	public Articulo guardar(Articulo articulo){
		if(articuloRepository.findByNombre(articulo.getNombre())!=null) {
			return null;
		}
		return articuloRepository.save(articulo);
	}
	
	public List<Articulo> listar(){
		return (List<Articulo>) articuloRepository.findAll();
	}
	
	public Articulo obtenerPorNombre(String nombre) {
		return articuloRepository.findByNombre(nombre);
	}
	public Articulo actualizar(Articulo articulo) {
		return articuloRepository.save(articulo);
	}
	
	public void eliminar(Articulo articulo) {
		articuloRepository.delete(articulo);
	}
}

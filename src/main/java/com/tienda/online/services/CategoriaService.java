package com.tienda.online.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tienda.online.models.Categoria;
import com.tienda.online.repositories.CategoriaRepository;

@Service
public class CategoriaService {

	private CategoriaRepository categoriaRepository;

	public CategoriaService(CategoriaRepository categoriaRepository) {
		this.categoriaRepository = categoriaRepository;
	}
	
	public Categoria guardar(Categoria categoria){
		if(categoriaRepository.findById(categoria.getNombre()).isPresent()) {
			return null;
		}
		return categoriaRepository.save(categoria);
	}
	
	public List<Categoria> listar(){
		return (List<Categoria>) categoriaRepository.findAll();
	}
	
	public Categoria actualizar(Categoria Categoria) {
		return categoriaRepository.save(Categoria);
	}
	
	public void eliminar(Categoria categoria) {
		categoriaRepository.delete(categoria);
	}
}
